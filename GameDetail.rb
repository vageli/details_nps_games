require 'active_record'
require 'fileutils'

ActiveRecord::Base.logger = Logger.new(STDERR)

ActiveRecord::Base.establish_connection(
  :adapter => "sqlite3",
  :database  => "psv.db"
)

ActiveRecord::Schema.define do
  unless ActiveRecord::Base.connection.tables.include? 'game_details'
    create_table :game_details do |table|
      table.column :description, :string
      table.column :cover, :string
      table.column :rating, :string
      table.column :addOns, :text, array: true
      table.column :themes, :text, array: true
      table.column :contentId, :string
      table.column :region, :string
      table.column :contentType, :string
      table.column :consoleType, :string
      table.column :fileType, :string
      table.column :genres, :text, array: true
      table.column :languages, :text, array: true
      table.column :size, :string
      table.column :platforms, :text, array: true
      table.column :publisher, :string
      table.column :publishDate, :time
    end
  end
end

class GameDetail < ActiveRecord::Base
  validates :contentId, uniqueness: true, presence: true
  serialize :addOns, Array
  serialize :themes, Array
  serialize :genres, Array
  serialize :languages, Array
  serialize :platforms, Array

  def rating=(*args)
    original = args[0]
    file_path = "ratings/#{args[0].gsub("https://cdn-a.sonyentertainmentnetwork.com/grc/images/ratings/hd/","")}"
    dirname = File.dirname(file_path)
    unless File.directory?(dirname)
      FileUtils.mkdir_p(dirname)
    end

    if File.exist?(file_path)
    else
      open("#{original}", "rb") do |image|
        File.write(file_path, image.read)
      end
    end

    super(file_path)
  end

  def cover=(*args)
    file_path = "#{consoleType}/#{region}/covers/#{contentId}.png"

    dirname = File.dirname(file_path)
    unless File.directory?(dirname)
      FileUtils.mkdir_p(dirname)
    end

    if File.exist?(file_path)
    else
      open("#{args[0]}?w=800&h=800", "rb") do |image|
        File.write(file_path, image.read)
      end
    end

    super(file_path)
  end
end
